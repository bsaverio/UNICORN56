function [X, R] = PVimpedance(mpc)
%PVimpedance	Computes impedance seen by the PV generators.
% 
%	[X, R] = PVimpedance(mpc)
%
%	returns the reactance and resistance matrices for the PV generators
%	relative to the substation (slack bus).
%
%	mpc              a matpower casefile processed by addPVfields

Y = makeYbus(mpc);

% compute X as in [doi: 10.1109/TAC.2013.2270317]

YY = zeros(mpc.N_BUSES + 1);
YY(1:mpc.N_BUSES,1:mpc.N_BUSES) = Y;
YY(mpc.SUBST_BUS,end) = 1;
YY(end, mpc.SUBST_BUS) = 1;

XX = inv(YY);
X = imag(XX(mpc.PV_BUS,mpc.PV_BUS));
R = real(XX(mpc.PV_BUS,mpc.PV_BUS));
