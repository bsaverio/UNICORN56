function mpc = addPVfields(mpc)
%addPVfields	Add useful fields to the MatPower casefile.

	% MatPower constants
	define_constants;
	
	% size of the grid and number of PV generators
	mpc.N_BUSES = size(mpc.bus,1);
	mpc.N_PVS = size(mpc.gen,1)-1;
	
	% substation and load buses
	mpc.SUBST_BUS = find(mpc.bus(:,BUS_TYPE)==REF);
	mpc.LOAD_BUS = find(mpc.bus(:,BUS_TYPE)~=REF);
	
	% PV generator indices/buses
	mpc.PV_I = find(mpc.gen(:,GEN_BUS)~=mpc.SUBST_BUS);
	mpc.PV_BUS = mpc.gen(mpc.PV_I,GEN_BUS);
	
	% PV limits
	mpc.PV_VMIN = mpc.bus(mpc.PV_BUS, VMIN);
	mpc.PV_VMAX = mpc.bus(mpc.PV_BUS, VMAX);
	mpc.PV_PMAX = mpc.gen(mpc.PV_I, PMAX);
	mpc.PV_QMIN = mpc.gen(mpc.PV_I, QMIN);
	mpc.PV_QMAX = mpc.gen(mpc.PV_I, QMAX);
	