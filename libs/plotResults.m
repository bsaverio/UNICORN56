function plotResults(mpc, gridtimeseries, p, q, v, plotTitle)

	% plot for two generators only
	if length(mpc.PV_I)~=2
		warning('Plot function for two generators only');
		return;
	end

	colorone = [217,95,2]/255;
	colortwo = [117,112,179]/255;
	colorgray = [200,200,200]/255;

	figure
	subplot(3,1,3)
		hold on
		line([0 gridtimeseries.t(end)/60/60], [0 0], 'Color', 'black', 'LineStyle', '-')
		plot(gridtimeseries.t/60/60, gridtimeseries.pvproduction(:,1), 'Color', colorone, 'LineStyle', '--')
		plot(gridtimeseries.t/60/60, gridtimeseries.pvproduction(:,2), 'Color', colortwo, 'LineStyle', '--')
		plot(gridtimeseries.t/60/60, p(:,1), 'Color', colorone, 'LineWidth', 1)
		plot(gridtimeseries.t/60/60, p(:,2), 'Color', colortwo, 'LineWidth', 1)
		xlabel('time [h]')
		ylabel('PV active [MW]')
		ylim([0 5])
		xlim([0 12])
		box on
	subplot(3,1,2)
		hold on
		line([0 gridtimeseries.t(end)/60/60], [mpc.PV_QMIN(1) mpc.PV_QMIN(1)], 'Color', colorone, 'LineStyle', '--')
		line([0 gridtimeseries.t(end)/60/60], [mpc.PV_QMIN(2) mpc.PV_QMIN(2)], 'Color', colortwo, 'LineStyle', '--')
		line([0 gridtimeseries.t(end)/60/60], [0 0], 'Color', 'black', 'LineStyle', '-')
		plot(gridtimeseries.t/60/60, q(:,1), 'Color', colorone, 'LineWidth', 1)
		plot(gridtimeseries.t/60/60, q(:,2), 'Color', colortwo, 'LineWidth', 1)
		xlabel('time [h]')
		ylabel('PV reactive [MVAR]')
		ylim([min(mpc.PV_QMIN)-0.1 0.1])
		xlim([0 12])
		box on
	subplot(3,1,1)
		hold on
		line([0 gridtimeseries.t(end)/60/60], [1 1], 'Color', 'black', 'LineStyle', '-')
		line([0 gridtimeseries.t(end)/60/60], [mpc.PV_VMAX(1) mpc.PV_VMAX(1)], 'Color', colorone, 'LineStyle', '--')
		line([0 gridtimeseries.t(end)/60/60], [mpc.PV_VMAX(2) mpc.PV_VMAX(2)], 'Color', colortwo, 'LineStyle', '--')
		plot(gridtimeseries.t/60/60,v, 'Color', colorgray, 'LineWidth', 0.5)
		plot(gridtimeseries.t/60/60,v(:,mpc.PV_BUS(1)), 'Color', colorone, 'LineWidth', 1)
		plot(gridtimeseries.t/60/60,v(:,mpc.PV_BUS(2)), 'Color', colortwo, 'LineWidth', 1)
		xlabel('time [h]')
		ylabel('voltage [p.u.]')
		ylim([0.98 1.1])
		xlim([0 12])
		box on

	sgtitle(plotTitle);
	set(gcf,'Position',[100 100 600 800]);
