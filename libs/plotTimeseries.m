function plotTimeseries(gridtimeseries)

	figure
	    hold on
	    line([0 gridtimeseries.t(end)/60/60], [0 0], 'Color', 'black', 'LineStyle', '-')
	    plot(gridtimeseries.t/60/60,gridtimeseries.pdemand, 'Color', 'black')
	    xlabel('time [h]')
	    ylabel('power demands [MW]')
	    xlim([0 12])
	    box on
	    
	% plot for two generators only
	if size(gridtimeseries.pvproduction,2)<2
		warning('Plot function for two generators only');
		return;
	end
	
	colorone = [217,95,2]/255;
	colortwo = [117,112,179]/255;

	figure
	    hold on
	    line([0 gridtimeseries.t(end)/60/60], [0 0], 'Color', 'black', 'LineStyle', '-')
	    plot(gridtimeseries.t/60/60,gridtimeseries.pvproduction(:,1), 'Color', colorone, 'LineWidth', 1)
	    plot(gridtimeseries.t/60/60,gridtimeseries.pvproduction(:,2), 'Color', colortwo, 'LineWidth', 1)
	    xlabel('time [h]')
	    ylabel('PV production [MW]')
	    xlim([0 12])
	    box on