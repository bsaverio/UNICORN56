function [p, q, v] = testController(mpc,controller,gridtimeseries)
%testController    Test a given controller with a given data series.
% 
%	[p, q, v] = testController(mpc, controller, gridtimeseries) 
%
%	returns three time series describing the active and reactive power
%	input of the PV generators, together with the voltage v of all buses.
%
%	mpc              a matpower casefile processed by addPVfields
%	controller       a controller struct 
%	gridtimeseries   time series of PV generation and demands

	% clear permament variables (internal controller states)
	eval(sprintf('clear %s', controller.strategy))

	% MatPower constants
	define_constants;

	p = zeros(length(gridtimeseries.t),2);
	q = zeros(length(gridtimeseries.t),2);
	v = zeros(length(gridtimeseries.t),mpc.N_BUSES);
	
	p(1,:) = gridtimeseries.pvproduction(1,1:mpc.N_PVS);
	
	for k = 1:length(gridtimeseries.t)
    
		% set PV injections
		mpc.gen(mpc.PV_I,PG) = p(k,1:mpc.N_PVS)';
		mpc.gen(mpc.PV_I,QG) = q(k,1:mpc.N_PVS)';
		
		% set power demands
		mpc.bus(mpc.LOAD_BUS,PD) = gridtimeseries.pdemand(k,:)';
		mpc.bus(mpc.LOAD_BUS,QD) = gridtimeseries.qdemand(k,:)';
    
		% solve PF equations
		results = runpf(mpc, mpoption('VERBOSE', 0, 'OUT_ALL',0));
    
		% save bus voltages
		v(k,:) = results.bus(:,VM);
    
		if k==length(gridtimeseries.t)
			break;
		end
		
		% call control strategy 
		[p(k+1,:), q(k+1,:)] = feval(controller.strategy, controller.parameters, p(k,:), q(k,:), v(k,mpc.PV_BUS));
		
		% clip power injection to feasible limits
		p(k+1,:) = min(max(p(k+1,:), 0), gridtimeseries.pvproduction(k+1,1:mpc.N_PVS));
		q(k+1,:) = min(max(q(k+1,:), mpc.PV_QMIN'), mpc.PV_QMAX');
		
	end

	