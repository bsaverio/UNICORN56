clear variables
close all
clc

% Conventions
% - rows = time
% - columns = index/agent/bus/node

% Add paths
addpath('data');
addpath('libs');
addpath('strategies');

% Assuming matpower is installed and added to path

%% Load grid model and add useful information

mpc = loadcase('case_unicorn56');
mpc = addPVfields(mpc);

%% Load time series and plot them

load gridtimeseries.mat
plotTimeseries(gridtimeseries);
    
%% A controller is a struct with two mandatory fields:
%  The field 'strategy' need to contain the function to be called in the
%  controller (from the strategies/ folder).
%  The field 'parameters' contains the strategy-dependent controller
%  parameters.

%% No control

controller_nocontrol = struct(						...
	'strategy',	'nocontrol',					...
	'parameters',	[]						);

[p_nocontrol, q_nocontrol, v_nocontrol] = testController(mpc,controller_nocontrol,gridtimeseries);
plotResults(mpc, gridtimeseries, p_nocontrol, q_nocontrol, v_nocontrol, 'No control');

%% Volt/VAr droop

parameters_droop = struct(						...
	'deadband',	0.01,						...
	'PV_VMIN',	mpc.PV_VMIN',					...
	'PV_VMAX',	mpc.PV_VMAX',					...
	'PV_QMIN',	mpc.PV_QMIN',					...
	'PV_QMAX',	mpc.PV_QMAX'					);

controller_droop = struct(						...
	'strategy',	'voltvardroop',					...
	'parameters',	parameters_droop				);

[p_droop, q_droop, v_droop] = testController(mpc,controller_droop,gridtimeseries);
plotResults(mpc, gridtimeseries, p_droop, q_droop, v_droop, 'Volt/VAR droop');

%% Feedback optimization 

[X, R] = PVimpedance(mpc);

parameters_fo = struct(							...
	'R',		R,						...
	'X',		X,						...
	'K',		10,						...
	'alpha',	30,						...
	'gamma',	0.1,						...
	'PV_VMIN',	mpc.PV_VMIN',					...
	'PV_VMAX',	mpc.PV_VMAX',					...
	'PV_QMIN',	mpc.PV_QMIN',					...
	'PV_QMAX',	mpc.PV_QMAX',					...
	'PV_PMAX',	mpc.PV_PMAX'					);

controller_fo = struct(							...
	'strategy',	'fo',						...
	'parameters',	parameters_fo					);

[p_fo, q_fo, v_fo] = testController(mpc,controller_fo,gridtimeseries);
plotResults(mpc, gridtimeseries, p_fo, q_fo, v_fo, 'Feedback optimization');

