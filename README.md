UNICORN 56-bus benchmark
========================

UNICORN56 is a benchmark for the problem of overvoltage mitigation in power distribution grids with distributed generation.

This benchmark has been prepared as part of the project
**UNICORN - A Unified Control Framework for Real-Time Power System operation**
https://unicorn.control.ee.ethz.ch/
co-sponsored by the Swiss Federal Office of Energy, by ETH Zurich, and by RTE.

**You are encouraged to used this code to test you voltage regulation strategy and to replicate the results of the paper**

- comparison.m generates the main plots that have been used in the paper.
- strategies/ contains possible control strategies, including the feedback optimization strategy develope in the project
- libs/ contains useful functions
- data/ contains the grid topology model and historical time series of PV generation and power demands

 UNICORN56 © 2021 is licensed under CC BY-ND 4.0.
 
