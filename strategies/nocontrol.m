function [nextp, nextq] = nocontrol(~, p, q, ~)
%nocontrol	No active power curtailment and no reactive power control.

	nextp = Inf(size(p));		% no curtailment
	nextq = zeros(size(q));		% no VAR compensation
	