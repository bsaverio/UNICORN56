function [nextp, nextq] = fo(parameters, p, q, v)
%fo	Feedback optimization controller (active and reactive power)
% 
%	The controller implements a dual-ascent iteration as in:
%
%	Saverio Bolognani, Ruggero Carli, Guido Cavraro, and Sandro Zampieri. 
%	On the need for communication for voltage regulation of power distribution grids.
%	IEEE Transactions on Control of Network Systems, 6(3), September 2019. 
%	https://doi.org/10.1109/TCNS.2019.2921268
%
%	Lukas Ortmann, Adrian Hauswirth, Ivo Caduff, Florian Dörfler, and Saverio Bolognani.
%	Experimental validation of feedback optimization in power distribution grids.
%	Electric Power Systems Research, 189:106782, December 2020.
%	https://doi.org/10.1016/j.epsr.2020.106782

	% internal controller state (dual multiplier)
	persistent lambda;
	
	% initialize at zero
	if isempty(lambda)
		lambda = zeros(size(v));
	end
	
	% dual gradient ascent step	
	lambda = max(0, lambda + parameters.alpha * (v - parameters.PV_VMAX));
	
	% inexact primal minimization via repeated primal gradient descent
	nextp = p;
	nextq = q;
	for innerk = 1:parameters.K
		nextq = max(parameters.PV_QMIN, nextq - parameters.gamma * (nextq + lambda * parameters.X));
		nextp = max(0, nextp - parameters.gamma * ((nextp-parameters.PV_PMAX) + lambda * parameters.R));
	end
	