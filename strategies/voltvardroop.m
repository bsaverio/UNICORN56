function [nextp, nextq] = voltvardroop(parameters, p, q, v)
%voltvardroop	Volt/VAr static compensation rule
% 
%	This static controller implements the static droop curve
%	recommended in many grid codes and standards, including the IEEE
%	standard 1547-2018.

	% no curtailment
	nextp = Inf(size(p)); 
	
	% unsaturated droop Volt/VAr curve
	nextq = (v>=1+parameters.deadband) .* (v-(1+parameters.deadband)) .* (parameters.PV_QMIN) ./ (parameters.PV_VMAX-(1+parameters.deadband)) + ...
		(v<1-parameters.deadband) .* (v-(1-parameters.deadband)) .* (parameters.PV_QMAX) ./ (parameters.PV_VMIN-(1-parameters.deadband));
